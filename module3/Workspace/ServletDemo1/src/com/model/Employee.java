package com.model;

public class Employee {
private int id;
private String empName;
private String emailId;
private String password;
private String gender;
private Double salary;
public Employee(){
	super();
}
public Employee(int id, String empName, String password,String gender, String emailId,  Double salary) {
	super();
	this.id = id;
	this.empName = empName;
	this.emailId = emailId;
	this.password = password;
	this.gender = gender;
	this.salary = salary;
}
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmpName() {
		return empName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Double getSalary() {
		return salary;
	}
	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public String toString() {
		return "Employee [id=" + id + ", empName=" + empName + ", emailId=" + emailId + ", password=" + password
				+ ", salary=" + salary + "]";
	}

}
