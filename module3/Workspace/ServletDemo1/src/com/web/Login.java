package com.web;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.EmployeeDao;
import com.model.Employee;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Login")
public class Login extends HttpServlet {     
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String emailId = request.getParameter("userName");
		String password = request.getParameter("Password");
		
		//Creating Session
		HttpSession session = request.getSession(true);
		session.setAttribute("emailId", emailId);

		out.println("<html>");
		out.println("<body bgcolor='lightyellow'><center>");
		out.println(emailId);
		if (emailId.equalsIgnoreCase("HR") && password.equals("HR")) {
			RequestDispatcher rd = request.getRequestDispatcher("HRHomePage.jsp");
			rd.forward(request, response);			
		} else {

			//For Employee login
			EmployeeDao empDao = new EmployeeDao();
			Employee emp = empDao.empLogin(emailId, password);
			
			if (emp != null) {
				RequestDispatcher rd = request.getRequestDispatcher("EmployeeHomePage");
				rd.forward(request, response);	
			} else {
				out.println("<h1 style='color:red;'>Invalid Credentials</h1>");			
				RequestDispatcher rd = request.getRequestDispatcher("Login.html");
				rd.include(request, response);		
			}			
			//For Employee login
		}

		out.println("</center></body></html>");		
	}


	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
//package com.web;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import com.dao.EmployeeDao;
//import com.model.Employee;
//
//@WebServlet("/Login")
//public class Login extends HttpServlet {
//	 
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		response.setContentType("text/html");
//		PrintWriter out=response.getWriter();
//	     String userName=request.getParameter("userName");
//	     String password=request.getParameter("password");
//	     EmployeeDao emp = new EmployeeDao();
//	     out.print("<html>");
//	     out.print("<body style = 'background-color:lightyellow' text ='green'>");
//	     if(userName.equals("HR")&&password.equals("HR")){ 
//	    	 RequestDispatcher rd = request.getRequestDispatcher("HRHomePage");
//	    	 rd.forward(request, response);
//	     }
//	     else if(emp!=null){
//	    	 RequestDispatcher rd = request.getRequestDispatcher("EmployeHomePage");
//	    	 rd.forward(request, response);
//	     }
//	     else{
//	    	 out.print("<h1>Invalid Credentials</h1>");
//	    	 RequestDispatcher rd = request.getRequestDispatcher("Login");
//	    	 rd.include(request, response);
//	     }
//	     out.print("</body>");  
//	     out.print("</html>");
//	}
//
//	 
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		doGet(request, response);
//	}
//
//}
//
