package com.web;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/EmployeeHomePage")
public class EmployeeHomePage extends HttpServlet {
	
	    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String emailId = request.getParameter("userName");
		
		out.println("<html>");
		out.println("<body bgcolor='lightyellow' text='green'>");
		
		//Providing Home & Logout Links
		out.println("<form align='right'>");
		out.println("<a href='EmployeeHomePage'>Home</a> &nbsp;");
		out.println("<a href='login.html'>Logout</a>");
		out.println("</form>");
		
		out.println("<h3>Welcome: " + emailId + "! </h3>");
		
		out.println("<center>");		
		out.println("<h1>Welcome to EmployeeHomePage</h1>");		
		out.println("</center></body></html>");
	}
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			doGet(request, response);
		}

	}