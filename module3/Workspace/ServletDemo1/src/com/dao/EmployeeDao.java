package com.dao;

import java.util.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.GetConnection;
import com.model.Employee;

public class EmployeeDao {
	public static Employee empLogin(String name, String password) {
		Connection con = GetConnection.getConnection();
		String querry = "select * from employee where empName=? AND password=? ";
		
		try {
			PreparedStatement pst= con.prepareStatement(querry) ;
			pst.setString(1, name);
			pst.setString(2, password);
			ResultSet rs=pst.executeQuery();// 101,deepika,9999.99,deepika@gmail.com,deepika@123

			Employee emp  = new Employee();
			if(rs.next()){
				emp.setId(rs.getInt(1));
				emp.setEmpName(rs.getString(2));
				emp.setPassword(rs.getString(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setSalary(rs.getDouble(6));
				return emp;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}     
		return null;
	}
public Employee getEmployeeById(int empId) {
		
		Connection con = GetConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String selectQry = "select * from employee where empId = ?";
		
		try {
			pst = con.prepareStatement(selectQry);
			pst.setInt(1, empId);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				
				Employee emp = new Employee();
				
				emp.setId(rs.getInt(1));
				emp.setEmpName(rs.getString(2));
				emp.setPassword(rs.getString(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setSalary(rs.getDouble(6));
				return emp;
				
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {			
			
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
public List<Employee> getAllEmployees() {
	Connection con = GetConnection.getConnection();
	PreparedStatement pst = null;
	ResultSet rs = null;
	
	String selectQry = "select * from employee";
	
	try {
		pst = con.prepareStatement(selectQry);
		rs = pst.executeQuery();
		
		List<Employee> empList = new ArrayList<Employee>();
		
		while (rs.next()) {
			
			Employee emp = new Employee();
			
			emp.setId(rs.getInt(1));
			emp.setEmpName(rs.getString(2));
			emp.setSalary(rs.getDouble(3));
			emp.setGender(rs.getString(4));
			emp.setEmailId(rs.getString(5));
			emp.setPassword(rs.getString(6));
			
			empList.add(emp);
		}
		
		return empList;
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {			
		
		if (con != null) {
			try {
				rs.close();
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	return null;
}
public int deleteEmployee(int empId) {
	// TODO Auto-generated method stub
	PreparedStatement pst = null;
	Connection con = GetConnection.getConnection();
		
	String deleteQry = "delete from employee where empId = ?";
		
	try {
		pst = con.prepareStatement(deleteQry);
		pst.setInt(1, empId);
			
		return pst.executeUpdate();	//returns 1 or 0
			
	} catch (SQLException e) {
		e.printStackTrace();
	}
		
	finally {			
			
		if (con != null) {
			try {
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
		
	return 0;
}
public int updateEmployee(Employee emp) {
	PreparedStatement pst = null;
	Connection con = GetConnection.getConnection();
		
	String updateQry = "update employee set empName=?, salary=?, gender=?, emailId=?, password=? where empId=?";
		
	try {
		pst = con.prepareStatement(updateQry);
			
		pst.setString(1, emp.getEmpName());
		pst.setDouble(2, emp.getSalary());
		pst.setString(3, emp.getGender());
		pst.setString(4, emp.getEmailId());
		pst.setString(5, emp.getPassword());
		pst.setInt(6, emp.getId());
			
		return pst.executeUpdate();	//returns 1 or 0
			
	} catch (SQLException e) {
		e.printStackTrace();
	}
		
	finally {			
			
		if (con != null) {
			try {
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
		
	return 0;
}




}


