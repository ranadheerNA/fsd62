package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDao;
import com.model.Product;

@RestController
public class ProductController {

	@Autowired		//Implementing Dependency Injection
	ProductDao productDao;
	
	@GetMapping("getAllProducts")
	public List<Product> getAllProducts() {		
		return productDao.getAllProducts();	
	}
	
	@GetMapping("getProductById/{id}")
	public Product getProductById(@PathVariable("id") int prodId) {
		return productDao.getProductById(prodId);
	}
	@GetMapping("getProductByName/{:name}")
	public List<Product> getProductByName(@PathVariable("name") String name) {
		return productDao.getProductByName(name);
	}
	
	@PostMapping("addProduct")
	public Product addProduct(@RequestBody Product prod) {
		return productDao.addProduct(prod);
	}
	
	@PutMapping("updateProduct")
	public Product updateProduct(@RequestBody Product prod) {
		return productDao.updateProduct(prod);
	}
	
	@DeleteMapping("deleteProductById/{id}")
	public String deleteProductById(@PathVariable("id") int prodId) {
		productDao.deleteProductById(prodId);
		return "Product Deleted Successfully!!";
	}
}



