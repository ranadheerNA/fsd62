package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDao {

	@Autowired	//Implementing Dependency Injection
	ProductRepository productRepository;

	public List<Product> getAllProducts() {
		return productRepository.findAll();
	}

	public Product getProductById(int prodId) {
		// TODO Auto-generated method stub
		return productRepository.findById(prodId).orElse(null);
		
	}

	public Product addProduct(Product prod) {
		// TODO Auto-generated method stub
		return productRepository.save(prod);
	}
	
	public Product updateProduct(Product prod) {
		// TODO Auto-generated method stub
		return productRepository.save(prod);
	}

	public void deleteProductById(int prodId) {
		// TODO Auto-generated method stub
		productRepository.deleteById(prodId);
	}

	public List<Product> getProductByName(String prodName) {
		return productRepository.findByName(prodName);
	}
	
}
