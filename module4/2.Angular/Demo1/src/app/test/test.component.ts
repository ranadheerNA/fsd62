import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {
id: number;
name: string;
age:number;
hobbies:any;
address:any;

  constructor() {
    // alert('Constructor Invoked...');
    this.id=101;
    this.name='ranadheer';
    this.age=25;
    this.hobbies=['Running','Swimming','reading','playing'];
    this.address = {streetNo: 101, city: 'Hyd', state: 'Telangana'};
  }

  ngOnInit() {
    // alert('ngOnInit Invoked...');
  }

}
