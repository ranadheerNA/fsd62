import { Component,OnInit } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.css'
})
export class Test2Component implements OnInit{
   
    person:any;
      constructor() {
        // alert('Constructor Invoked...');
       
        this.person={
    
          "id":"101",
          "name":"ranadheer",
          "age":25,
          "hobbies":['Running','Swimming','reading','playing'],
          "address" : {streetNo: 101, city: 'Hyd', state: 'Telangana'}
        }
      }
    
      ngOnInit() {
        // alert('ngOnInit Invoked...');
      }
      submit() {
        // alert("Id:" + this.person.id + "\n" + "Name: " + this.person.name);
        console.log(this.person);
      }
    
    
}
