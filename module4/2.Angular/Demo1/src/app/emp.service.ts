import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
//Dependency Injection for HttpClientqwxdsqzsasxqwxdsw2xdssq saa
isUserLoggedIn: boolean;
loginStatus: any;
private cartCountSubject = new Subject<number>();
  cartCount$ = this.cartCountSubject.asObservable();


constructor(private http: HttpClient) {
  this.isUserLoggedIn = false;
 
  this.loginStatus = new Subject();
}

setUserLoggedIn() {
  this.isUserLoggedIn = true;
  this.loginStatus.next(true);
}
setUserLoggedOut() {
  this.isUserLoggedIn = false;
  this.loginStatus.next(false);
}

getUserLoginStatus(): any {
  return this.loginStatus.asObservable();
}

getAllContries() {
  return this.http.get('https://restcountries.com/v3.1/all');
}
employeeLogin(emailId: any, password: any) {
  return this.http.get('http://localhost:8085/empLogin/' + emailId + "/" + password).toPromise();
}
getEmployeeById(empId: any) {
  return this.http.get('http://localhost:8085/getEmployeeById/' + empId).toPromise();
}
deleteEmployeeById(empId: any) {
  return this.http.delete('http://localhost:8085/deleteEmployeeById/' + empId);
}
updateEmployee(employee: any) {
  return this.http.put('http://localhost:8085/updateEmployee', employee);
}
// setUserLoggedIn() {
//   this.isUserLoggedIn = true;
//   this.loginStatus.next(true);
// }

// getUserLoginStatus(): any {
//   return this.loginStatus.asObservable();
// }

// setUserLoggedOut() {
//   this.isUserLoggedIn = false;
// }
getLoginStatus(): boolean {
  return this.isUserLoggedIn;
}
getAllEmployees() {
  return this.http.get('http://localhost:8085/getAllEmployees');
}
getAllProducts() {
  return this.http.get('http://localhost:8085/getAllProducts');
}
registerEmployee(employee: any) {
  return this.http.post('http://localhost:8085/addEmployee', employee);
}
getAllDepartments(){
return this.http.get('http://localhost:8085/getAllDepartments');
}
updateCartCount(count: number) {
  this.cartCountSubject.next(count);
  }


}
