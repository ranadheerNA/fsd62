import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

//Edit Employee
declare var jQuery: any;
@Component({
  selector: 'app-show-employee',
  templateUrl: './show-employee.component.html',
  styleUrl: './show-employee.component.css'
})

export class ShowEmployeeComponent implements OnInit  {
 
  employees: any; 
  emailId: any;

  //Edit Employee
  editEmp: any;
  departments: any;
  countries: any;
  //Edit Employee

  constructor(private service: EmpService)   {
    this.emailId = localStorage.getItem('emailId');

    //Edit Employee
    this.editEmp = {
      empId: '',
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',

      department: {
        deptId: ''
      }
    }
    //Edit Employee
  }

  ngOnInit() {
    this.service.getAllEmployees().subscribe((data: any) => {this.employees = data;});

    //Edit Employee
    this.service.getAllContries().subscribe((data: any) => {this.countries = data;});
    this.service.getAllDepartments().subscribe((data: any) => {this.departments = data;});
    //Edit Employee
  }

  //Edit Employee
  editEmployee(emp: any) {
    this.editEmp = emp;
    jQuery('#editEmp').modal('show');
  }

  //Edit Employee
  updateEmployee() {
    this.service.updateEmployee(this.editEmp).subscribe((data: any) => {
      console.log(data);
    });
  }

  deleteEmployee(emp: any) {
    this.service.deleteEmployeeById(emp.empId).subscribe((data: any) => {
      console.log(data);
    });

    const i = this.employees.findIndex((employee: any) => {
      return emp.empId == employee.empId;
    });

    this.employees.splice(i, 1);

  }

}
