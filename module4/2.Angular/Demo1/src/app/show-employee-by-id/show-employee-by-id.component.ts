import { Component } from '@angular/core';
import { EmpService } from '../emp.service';
@Component({
  selector: 'app-show-employee-by-id',
  templateUrl: './show-employee-by-id.component.html',
  styleUrl: './show-employee-by-id.component.css'
})
export class ShowEmployeeByIdComponent {

  emp: any;
  msg: string;
  emailId: any;

  constructor(private service: EmpService) {
    this.msg = "";
    this.emailId = localStorage.getItem('emailId');
  }

  async getEmployee(employee: any) {
    this.msg = "";
    this.emp = null;
   
    await this.service.getEmployeeById(employee.empId).then((data: any) => {
      console.log(data);
      this.emp = data;
    });
   
    if (this.emp == null) {
      this.msg = "Employee Record Not Found!!!";
    }   
  }
 
}
