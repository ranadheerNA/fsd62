import { Component,OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit {
  loginStatus: any;
  cartCount: number = 0; // Define cartCount property

  constructor(private  service: EmpService, router: Router) {
    // Subscribe to cart count changes
  this.service.cartCount$.subscribe(count => {
    this.cartCount = count;
    });
   
  //  this.products  = .getItem('cartProducts');
   
  }

  ngOnInit() {
    
    this.service.getUserLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });
  }
 

  }


