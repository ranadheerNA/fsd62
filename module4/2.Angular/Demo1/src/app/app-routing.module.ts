import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShowEmployeeByIdComponent } from './show-employee-by-id/show-employee-by-id.component';
import { ShowEmployeeComponent } from './show-employee/show-employee.component';
import { LogoutComponent } from './logout/logout.component';
import { authGuard } from './auth.guard';
import { ProductComponent } from './product/product.component';
import { AddToCartComponent } from './add-to-cart/add-to-cart.component';
const routes: Routes = [
  {path:'',            component:LoginComponent},
  {path:'login',       component:LoginComponent},
  {path:'register',    component:RegisterComponent},
  {path:'showemps',    canActivate:[authGuard],component:ShowEmployeeComponent},
  {path:'showempbyid', canActivate:[authGuard],component:ShowEmployeeByIdComponent},
   {path:'logout',      canActivate:[authGuard],component:LogoutComponent},
   {path:'products',      canActivate:[authGuard],component:ProductComponent},
   {path:'cart',      canActivate:[authGuard],component:AddToCartComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
