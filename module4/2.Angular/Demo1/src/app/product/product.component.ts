import { Component } from '@angular/core';
import { EmpService } from '../emp.service';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent {
  products: any;
  emailId: any;
  cartProducts: any;

  constructor(private service: EmpService) {

    //Making the CartProducts as an Array of Products
    this.cartProducts = [];
    this.emailId = localStorage.getItem('emailId'); 
  }
  ngOnInit(): void {
    this.service.getAllProducts().subscribe((data:any)=>{
      console.log(data);
      this.products=data;
    })
  }

  addToCart(product: any) {
    this.cartProducts.push(product);
    this.service.updateCartCount(this.calculateCartCount());
    localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));
  }
private calculateCartCount(): number {
  // Calculate the count of items in the cart
  return this.cartProducts.length;
  }

}
