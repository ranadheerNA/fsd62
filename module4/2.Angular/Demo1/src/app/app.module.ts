import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './test2/test2.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { ShowEmployeeComponent } from './show-employee/show-employee.component';
import { ShowEmployeeByIdComponent } from './show-employee-by-id/show-employee-by-id.component';
import { ExpPipe } from './exp.pipe';
import { ExpComponent } from './exp/exp.component';

import { GenderPipe } from './gender.pipe';
import { LogoutComponent } from './logout/logout.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { AddToCartComponent } from './add-to-cart/add-to-cart.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    Test2Component,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    ShowEmployeeComponent,
    ShowEmployeeByIdComponent,
    ExpPipe,
    ExpComponent,
    
    GenderPipe,
          LogoutComponent,
          ProductComponent,
          AddToCartComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule
  ],
  providers: [
    provideClientHydration()
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  
 }
