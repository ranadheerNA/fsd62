import { JsonPipe } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';


@Component({
  selector: 'app-add-to-cart',
  templateUrl: './add-to-cart.component.html',
  styleUrl: './add-to-cart.component.css'
})
export class AddToCartComponent {
  cartProducts: any;
  products: any;
  emailId: any;
  cartCount:number;
  total: number;
  
  constructor(private router: Router, service: EmpService ) {
    this.cartCount = 0;

    this.total = 0;

    this.emailId = localStorage.getItem('emailId');
    this.products  = localStorage.getItem('cartProducts');
    this.cartProducts = JSON.parse(this.products);

    console.log(this.cartProducts);

    

    if (this.cartProducts != null) {
     
      this.cartProducts.forEach((element: any) => {
        this.total += element.price;
      });
    }
  }
// if( products!=null){

// }
  purchase() {

    alert('Thank You for Purchasing');

    this.cartProducts = [];
    localStorage.removeItem('cartProducts');
    this.router.navigate(['products']);
  }

}
